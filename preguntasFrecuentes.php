<!DOCTYPE html>
<html lang="en">
<head> 
   <meta name="ms.loc" content="ar">
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="https://products.office.com/CMSImages/favicon.png?version=8a5247a4-aa03-c2d9-d3e0-c0a606c2c9f9" type="image/png">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta property="og:description" content="La forma de estudiar evolucionó. Microsoft acerca Office 365 a alumnos y docentes para que puedan editar, compartir y almacenar el trabajo de todos los días en forma gratuita." />
   <meta property="og:image"       content="https://www.quieromioffice.com/images/compartir.png"/>
   <meta property="og:title" content="Office 365 GRATIS para alumnos y docentes." />
   <title>Quiero mi Office</title>
   <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
   <link rel="stylesheet" href="assets/style_type.css">
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/font_css.css">
   <link rel="stylesheet" href="assets/owl.carousel.css">
   <link rel="stylesheet" href="assets/owl.theme.css">
   <link rel="stylesheet" href="assets/preguntasFrecuentes.css">
   <!--   <link rel="stylesheet" href="assets/style_mobile.css" media="handheld, only screen and (max-device-width:720px)"> -->
   <!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
   <style id="antiClickjack">body{display:none !important;}</style>
   <script type="text/javascript">
      if (self === top) {
         var antiClickjack = document.getElementById("antiClickjack");
         antiClickjack.parentNode.removeChild(antiClickjack);
      } else {
         top.location = self.location;
      }
   </script>

</head>

<body>
   <?php include("nav.php"); ?>
   <!-- OFFICE FORMULARIO -->
   <div class="container-fluid office-preg">
      <section class="formulario-office preg-frecuentes" id="formulario-office">
         <div class="row">
            <div class="col-xs-6 col-offset-xs-1 col-sm-3 col-offset-sm-1  col-md-2">
               <img src="images/ilustracion_preguntas.png" alt="ilustración form" class="img-responsive img1">
            </div>
            <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-1 col-md-9">
               <!-- FORM  -->
               <div class="row sarasa">
                  <div class="col-md-10 mostrar_finish">
                     <p>Sus datos fueron enviados correctamente.
                     <br> Mientras nos contactamos con usted puede descargar una versión de prueba haciendo <a href="http://officeboxes.azurewebsites.net/" target="_blank">click aquí.</a> </p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="subTitulo">
                              <img src="images/signoPregunta.png">
                              <p>¿Quiénes pueden acceder al beneficio?</p>
                           </div>
                           <div class="row">
                              <div class="col-sm-11 col-offset-sm-1 respuesta">
                              <img src="images/triangle.png"> 
                              <p>Cualquier institución educativa que tenga licenciado el paquete de Office para todos sus administrativos con algún plan de suscripción.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12">
                           <div class="subTitulo">
                              <img src="images/signoPregunta.png">
                              <p>¿En qué consiste Office ProPlus?</p>
                           </div>
                           <div class="row">
                              <div class="col-sm-11 col-offset-sm-1 respuesta">
                                 <img src="images/triangle.png"> 
                                 <p>Se le podrá ofrecer a todos los alumnos y docentes de la institución la posibilidad de adquirir Office ProPlus sin costo alguno, con la posibilidad de instalarlo hasta en 15 dispositivos (tanto PC y notebooks, como tablets y teléfonos).</p>                                   
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-12 subTitulo">
                           <div class="subTitulo">
                              <img src="images/signoPregunta.png">
                              <p>¿Cómo se puede suscribir una escuela para obtener el Office para los alumnos y docentes sin costo adicional?</p>
                           </div>
                           <div class="row">
                              <div class="col-sm-11 col-offset-sm-1 respuesta">
                              <img src="images/triangle.png"> 
                              <p>Las escuelas pueden suscribirse a través de sus programas Enrollment for Educations Solutions(EES) y Open Value Subscription-Education Solutions (OVS-ES), o su contrato institucional existente.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div id="loading"><img src="images/loading.gif" alt=""></div>
   </div>
   <!-- Conoce más beneficios DESKTOP -->
   <div class="version_desktop">
      <div class="col-md-12 conoce_beneficios text-right">
         <h3>Conocé más beneficios para estudiantes:  </h3>
      </div>
      <div class=" cuadro_beneficios">
         <div class="col-xs-4 col-sm-4 col-md-4 cuadro_beneficios1">
            <img src="images/bg_beneficios.png" alt="Ilustración Beneficios" class="imagen-beneficios">
         </div>
         <div class="col-xs-8 col-sm-8 col-md-8">
           <!-- <div class="row margin-top-cuadro marg-row-1">
              <div class="col-xs-3 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-4"><a href="http://www.azureboxes.com" target="_blank"  title="AzureBoxes">AzureBoxes</a></div>
              <div class=" col-xs-4  col-sm-4 col-md-4"><a href="http://www.microsoft.com/bizspark" target="_blank" style="padding-left: 20px;" title="Bizspark">Bizspark</a></div>
              <div class="col-xs-4 col-sm-4 col-md-4"><a href="http://kodulab.azurewebsites.net/" target="_blank"  title="Kodu">Kodu </a></div>
           </div> -->

           <div class="row margin-top-cuadro linkConoceMas">

               <a href="http://www.azureboxes.com" target="_blank"  title="AzureBoxes">AzureBoxes</a>
               <a href="http://www.microsoft.com/bizspark" target="_blank" title="Bizspark">Bizspark</a>
               <a href="http://kodulab.azurewebsites.net/" target="_blank"  title="Kodu">Kodu </a>
           </div>
           <div class="row padding-top linkConoceMas2" >
               <a href="http://www.dreamspark.com" target="_blank" title="Dreamspark">Dreamspark</a>
               <a href="http://officeboxes.azurewebsites.net/" target="_blank" title="OfficeBoxes">OfficeBoxes</a>
           </div>
         </div>   
      </div>
   </div>  
   <!-- FIN Conoce más beneficios DESKTOP -->
   <!-- BENEFICIOS MOBILE-->
   <div class="version_mobile">
      <div class="col-md-12 conoce_beneficios text-right">
         <h3>Conocé más beneficios:  </h3>
      </div>
      <div class=" cuadro_beneficios">
         <div class="col-xs-3 col-sm-4 col-md-4 cuadro_beneficios1">
            <img src="images/bg_beneficios.png" alt="Imágen Beneficios" class="imagen-beneficios">
         </div>
         <div class="col-xs-8 col-xs-offset-1 col-sm-8 col-md-8 marg-row-1">
           <div class="row margin-top-cuadro linkConoceMas">

               <a href="http://www.azureboxes.com" target="_blank"  title="AzureBoxes">AzureBoxes</a>
               <a href="http://www.microsoft.com/bizspark" target="_blank" title="Bizspark">Bizspark</a>
               <a href="http://kodulab.azurewebsites.net/" target="_blank"  title="Kodu">Kodu </a>
           </div>
           <div class="row padding-top linkConoceMas2" >
               <a href="http://www.dreamspark.com" target="_blank" title="Dreamspark">Dreamspark</a>
               <a href="http://officeboxes.azurewebsites.net/" target="_blank" title="OfficeBoxes">OfficeBoxes</a>
           </div>
         </div>   
      </div>
   </div>
   <!-- FIN BENEFICIOS MOBILE-->
   <!-- LEGALES: oferta válida -->
    <div class="col-xs-12 legales">
      <p><b>*</b>OFERTA VÁLIDA PARA DISPOSITIVOS CON WINDOWS 7 SP1 Y WINDOWS 8.1 POR UN AÑO DESDE EL 29 DE JULIO DE 2015. LOS TÉRMINOS Y CONDICIONES APLICABLES A LA REFERIDA ACTUALIZACIÓN SE ENCUENTRAN DISPONIBLES EN: <a style="color: #000;     font-family: wf_segoe-ui_semibold;" href="https://www.microsoft.com/es-es/software-download/windows10" target="_blank">AKA.MS/WIN10ACTUALIZACION</a>. CONSULTAR REQUERIMIENTOS TÉCNICOS PARA ACTUALIZAR A WINDOWS 10 <a href="http://www.microsoft.com/es-ar/windows/windows-10-upgrade" target="_blank"><u>AQUÍ</u></a>.</p>
    </div>
   <!-- FIN LEGALES: oferta válida -->

   <!-- FOOTER -->
   <footer>
      <div class="container foo-mobile">
         <div class="pull-right-1">
            <img src="images/logo_microsoft.png" alt="Powered by Microsoft Azure" width="104">
               <p> @ 2016 Microsoft</p>   
         </div>
      <div class="links-margin">
         <a href="http://go.microsoft.com/fwlink/p/?LinkID=511196">Contáctenos</a>
         <a href="http://www.microsoft.com/privacystatement/es-xl/core/default.aspx">Privacidad</a>
         <a href="http://products.office.com/es-ar/products">Todos los productos</a> 
         <a href="http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx">Marcas Registradas</a>
         <a href="http://go.microsoft.com/fwlink/p/?LinkID=511200">Sobre nuestra publicidad</a>
      </div>
      </div>
  </footer>
   <!-- FIN FOOTER -->

   <!-- MODAL -->
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <div class="row">
                  <div class="col-sm-5 cuadro_contactanos">
                     <p>¿Tenés preguntas? <span id="contactanos">Contactanos</span></p>
                  </div>
                  <div class="col-sm-7 cuadro_contactanos2">
                     <p id="mas_info">Para más información,<br><span>escribinos a <b>mioffice@microsoft.com</b> </span></p>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <div class="text-center">
                  <a href="mailto:mioffice@microsoft.com">
                     <button type="button" class="btn btn-default">Envia Tu Consulta</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--FIN MODAL --> 
   <!--JAVASCRIPTS-->
   <script src="assets/jquery.2.1.3.min.js"></script>
   <!--  JQUERY 2.1.3-->
   <script src="assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="assets/js/app.js"></script>
   <script src="funciones_validarform.js"></script>
   <script src="assets/ajax.js"></script>

   <!-- INICIO WEDECX & GOOGLE CODE  -->
   <script type="text/javascript">
      var varSegmentation = 0;
      var varClickTracking = 1;
      var varCustomerTracking = 1;
      var varAutoFirePV = 1;
      var Route = "";
      var Ctrl = ""
      document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
   </script>

   <noscript>
      <img src="http://c.microsoft.com/trans_pixel.aspx" id="ctl00_MscomBI_ctl00_ImgWEDCS" width="1" height="1" />
   </noscript>
   <!--  FIN WEDECX & GOOGLE CODE -->

   <!-- Google Analytics -->
   <script>
      (function(i, s, o, g, r, a, m) {
         i['GoogleAnalyticsObject'] = r;
         i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date();
         a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
         a.async = 1;
         a.src = g;
         m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

      ga('create', 'UA-74673865-1', 'auto');
      ga('send', 'pageview');
   </script>
   <!-- Fin Google Analytics -->
   <!-- Facebook Pixel Code -->
   <script>
      ! function(f, b, e, v, n, t, s) {
         if (f.fbq) return;
         n = f.fbq = function() {
            n.callMethod ?
               n.callMethod.apply(n, arguments) : n.queue.push(arguments)
         };
         if (!f._fbq) f._fbq = n;
         n.push = n;
         n.loaded = !0;
         n.version = '2.0';
         n.queue = [];
         t = b.createElement(e);
         t.async = !0;
         t.src = v;
         s = b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t, s)
      }(window,
         document, 'script', '//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '485341735003297');
      fbq('track', "PageView");
   </script>
   <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=485341735003297&ev=PageView&noscript=1" />
   </noscript>
   <!-- End Facebook Pixel Code -->
</body>

</html>