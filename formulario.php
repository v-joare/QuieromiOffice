<?php require "conexion.php"; ?>
<!DOCTYPE html>  
<html lang="en">
<head> 
   <meta name="ms.loc" content="ar">
   <meta charset="UTF-8">
   <link rel="shortcut icon" href="https://products.office.com/CMSImages/favicon.png?version=8a5247a4-aa03-c2d9-d3e0-c0a606c2c9f9" type="image/png">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta property="og:description" content="La forma de estudiar evolucionó. Microsoft acerca Office 365 a alumnos y docentes para que puedan editar, compartir y almacenar el trabajo de todos los días en forma gratuita." />
   <meta property="og:image" content="https://www.quieromioffice.com/images/compartir.png" />
   <meta property="og:title" content="Office 365 GRATIS para alumnos y docentes." />
   <title>Quiero mi Office</title>
   <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
   <link rel="stylesheet" href="assets/style_type.css">
   <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/font_css.css">
   <link rel="stylesheet" href="assets/owl.carousel.css">
   <link rel="stylesheet" href="assets/owl.theme.css">
   <!--   <link rel="stylesheet" href="assets/style_mobile.css" media="handheld, only screen and (max-device-width:720px)"> -->
   <!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
   <style id="antiClickjack">body{display:none !important;}</style>
   <script type="text/javascript">
      if (self === top) {
         var antiClickjack = document.getElementById("antiClickjack");
         antiClickjack.parentNode.removeChild(antiClickjack);
      } else {
         top.location = self.location;
      }
   </script>
</head>

<body class="formulario">
   <?php include("nav.php"); ?>
   <!-- MODAL -->
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-body">
               <div class="row">
                  <div class="col-sm-5 cuadro_contactanos">
                     <p>¿Tenés preguntas?<span id="contactanos">Contactanos</span>
                     </p>
                  </div>
                  <div class="col-sm-7 cuadro_contactanos2">
                     <p id="mas_info">Para más información,
                        <br><span>escribinos a <b>mioffice@microsoft.com</b> </span>
                     </p>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <div class="text-center">
                  <a href="mailto:mioffice@microsoft.com">
                     <button type="button" class="btn btn-default">Envia Tu Consulta</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--FIN MODAL -->
   <!-- OFFICE FORMULARIO -->
   <div class="centrar">
      <section class="formulario-office" id="formulario-office">
         <div class="row">
            <div class="col-xs-6 col-offset-xs-3 col-md-2">
               <img src="images/ilustracion_form.png" alt="ilustración form" class="img-responsive img1">
            </div>
            <div class="col-md-10">
               <!-- FORM  -->
               <div class="row">
                  <div class="col-md-12 mostrar1">
                     <p>Si su institución actualmente no brinda este beneficio a los alumnos y desea adherirse al programa, complete el siguiente formulario con los datos de la institución para que podamos contactarlo.</p>
                  </div>
                  <div class="col-md-10 mostrar_finish">
                     <p>Sus datos fueron enviados correctamente.
                        <br> Mientras nos contactamos con usted puede descargar una versión de prueba haciendo <a href="http://officeboxes.azurewebsites.net/" target="_blank">click aquí.</a> </p>
                  </div>
               </div>
               <div class="row">
                  <form id="fo3" action="form.php" method="post" name="fo3" class="form-horizontal" role="form">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="nombre"><img src="images/triangle.png" alt="triangle">Nombre</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="nombre" name="nombre" tabindex=1  placeholder="Ingrese Nombre ">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="email"><img src="images/triangle.png" alt="triangle">Email</label>
                           <div class="col-sm-9">
                              <input type="email" class="form-control" id="email" name="email" tabindex=3  placeholder="Ingrese email">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="institucion"><img src="images/triangle.png" alt="triangle">Institución</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="institucion" name="institucion" tabindex=5 placeholder="Ingrese Institución ">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6 margen-bottom">
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="apellido"><img src="images/triangle.png" alt="triangle">Apellido</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="apellido" name="apellido" tabindex=2 placeholder="Apellido">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="telefono"><img src="images/triangle.png" alt="triangle">Teléfono</label>
                           <div class="col-sm-9">
                              <input type="tel" class="form-control" id="telefono" name="telefono"  tabindex=4 placeholder="+54 911 XXXX XXXX">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="control-label col-sm-2 col-sm-offset-1" for="cargo"><img src="images/triangle.png" alt="triangle">Cargo</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="cargo" name="cargo" tabindex=6 placeholder="Ingrese Cargo">
                           </div>
                        </div>
                        <button type="submit" class="col-xs-12 col-sm-9 col-sm-offset-3 cta-enviar">Enviar<img src="images/arrow.png" alt="images">
                        </button>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </section>
      <div id="loading"><img src="images/loading.gif" alt=""></div>
   </div>
   <!-- OFFICE FORMULARIO -->
   <!-- FOOTER -->
   <!-- <footer class="footer_form foo-pos-abajo">
      <div class="container ">
         <div class="pull-right-1">
            <img src="images/logo_microsoft.png" alt="Powered by Microsoft Azure" width="104">
            <p> © 2016 Microsoft</p>
         </div>
         <div class="links-margin">
            <a href="http://go.microsoft.com/fwlink/p/?LinkID=511196">Contáctenos</a>
            <a href="http://www.microsoft.com/privacystatement/es-xl/core/default.aspx">Privacidad</a>
            <a href="http://products.office.com/es-ar/products">Todos los productos</a>
            <a href="http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx">Marcas Registradas</a>
            <a href="http://go.microsoft.com/fwlink/p/?LinkID=511200">Sobre nuestra publicidad</a>
         </div>
      </div>
   </footer> -->
   <!-- FIN FOOTER -->
   <!--JAVASCRIPTS-->
   <script src="assets/jquery.2.1.3.min.js"></script>
   <!--  JQUERY 2.1.3-->
   <script src="assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="assets/js/app.js"></script>
   <script src="funciones_validarform.js"></script>
   <script src="assets/ajax.js"></script>

   <!-- INICIO WEDECX & GOOGLE CODE  -->
   <script type="text/javascript">
      var varSegmentation = 0;
      var varClickTracking = 1;
      var varCustomerTracking = 1;
      var varAutoFirePV = 1;
      var Route = "";
      var Ctrl = ""
      document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
   </script>
   <noscript><img src="http://c.microsoft.com/trans_pixel.aspx" id="ctl00_MscomBI_ctl00_ImgWEDCS" width="1" height="1" />
   </noscript>
   <!--  FIN WEDECX & GOOGLE CODE -->

   <!-- Google Analytics -->
   <script>
      (function(i, s, o, g, r, a, m) {
         i['GoogleAnalyticsObject'] = r;
         i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
         }, i[r].l = 1 * new Date();
         a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
         a.async = 1;
         a.src = g;
         m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

      ga('create', 'UA-74673865-1', 'auto');
      ga('send', 'pageview');
   </script>
   <!-- Fin Google Analytics -->
   <!-- Facebook Pixel Code -->
   <script>
      ! function(f, b, e, v, n, t, s) {
         if (f.fbq) return;
         n = f.fbq = function() {
            n.callMethod ?
               n.callMethod.apply(n, arguments) : n.queue.push(arguments)
         };
         if (!f._fbq) f._fbq = n;
         n.push = n;
         n.loaded = !0;
         n.version = '2.0';
         n.queue = [];
         t = b.createElement(e);
         t.async = !0;
         t.src = v;
         s = b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t, s)
      }(window,
         document, 'script', '//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '485341735003297');
      fbq('track', "PageView");
   </script>
   <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=485341735003297&ev=PageView&noscript=1" />
   </noscript>
   <!-- End Facebook Pixel Code -->
</body>

</html>