$(document).ready(function() {
   // Esta primera parte crea un loader no es necesaria
    $(document).ajaxStart(function() {
        $('#loading').show();
        $('.mostrar_finish').hide();
    }).ajaxStop(function() {
        $('#loading').hide();
        $('#fo3').hide();
        $('.mostrar1').hide();
        $('.mostrar_finish').fadeIn('slow');
    });
    $('#fo3').submit(function() {
   // Interceptamos el evento submit
  // Enviamos el formulario usando AJAX
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            // Mostramos un mensaje con la respuesta de PHP
            success: function(data) {
               
            }
        })    
        return false;
    }); 
})
// ]]></script>