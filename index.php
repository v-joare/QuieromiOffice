<?php
   require "conexion.php";
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta name="ms.loc" content="ar">
      <meta charset="UTF-8">
      <link rel="shortcut icon" href="https://products.office.com/CMSImages/favicon.png?version=8a5247a4-aa03-c2d9-d3e0-c0a606c2c9f9" type="image/png">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta property="og:description" content="La forma de estudiar evolucionó. Microsoft acerca Office 365 a alumnos y docentes para que puedan editar, compartir y almacenar el trabajo de todos los días en forma gratuita." />
      <meta property="og:image" content="https://www.quieromioffice.com/images/compartir.png"/>
      <meta property="og:title" content="Office 365 GRATIS para alumnos y docentes." />
      <title>Quiero mi Office</title>
      <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
      <link rel="stylesheet" href="assets/style_type.css">
      <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/font_css.css">
      <link rel="stylesheet" href="assets/owl.carousel.css">
      <link rel="stylesheet" href="assets/owl.theme.css">
    <!--   <link rel="stylesheet" href="assets/style_mobile.css" media="handheld, only screen and (max-device-width:720px)"> -->
      <!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
      <style id="antiClickjack">body{display:none !important;}</style>
      <script type="text/javascript">
         if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
         } else {
            top.location = self.location;
         }
      </script>
   </head>
   <body>
      <?php include("nav.php"); ?>

      <div class="container-fluid office1" id="office1">
         <div class="row color_fondo">
            <!--  
            BANNER OFFICE RACE
            -->
            <div class="col-xs-12 col-md-5 col-lg-4 office_race">
               <!-- <img src="images/proximamente.png" class="run_office top_race"> -->
               <img src="images/logo_race.png" alt="" class="logo_race">
               <h3>¡La gran carrera para tu carrera!</h3>
               <p>
                  Sumate al desafío para que tu institución gane el Microsoft Imagine Academy y puedas acceder a certificarte con las últimas tecnologías.
               </p>
               <img src="images/running.png" alt="" class="run_office img-responsive bottom_race">
               <div class="button_office_race col-md-5 col-centered"><a href="office_race/">Participá acá  </a></div>
            </div>
            <!--  
               IMAGENES OFFICE y paso 1 | paso 2
            -->
            <div class="col-xs-12 col-md-7 col-lg-8">
               <!--  
                  IMAGENES OFFICE
               -->
               <div class="row row-prnci">
                  <div class="col-xs-12 col-sm-12 office_gratis text-center">
                     <div class="col-xs-6 col-md-6">
                        <img src="images/imagen_banner.png" alt="Ilustración Banner Office 365" class="img-responsive ubi-respo">
                     </div>
                     <div class="col-xs-6 col-md-6 text-center text-respo">
                        <h1>Office gratis para <div class="hidden-xs" style="height:1px!important"><br></div>
                           Alumnos y Docentes.
                        </h1>
                        <h3>Con tu cuenta institucional <div class="lala" style="display:none"><br></div>podés <div class="lala2" ><br></div>
                           descargar Office gratis<div class="lala" style="display:none"><br></div> en solo 2 pasos.
                        </h3>
                     </div>
                  </div>
               </div>

               <div class="row pasos">
                  <!-- PASO 1 -->
                  <div class="col-xs-12 col-sm-6 sec-col-01">
                     <div class="col-xs-2">
                        <div class="paso1 text-right">Paso 1</div>
                        <div class="signo text-right sgno_pas" >?</div>
                     </div>
                     <div class="col-xs-10 col-sm-10 ingresa_institucion">
                        <div class="row">
                           <h3>Ingresá tu correo institucional <br>para obtener tu Office</h3>
                        </div>
                     </div>
                     <div class="col-xs-12 botonesdescarga container">

                           <a href="https://portal.office.com/start?sku=e82ae690-a2d5-4d76-8d30-7c6e01e6022e" target="_blank" title="Haciendo Click Aquí">
                           <div class="descarga_office otro text-left"> HACIENDO CLIC AQUÍ</div></a>

                     </div>
                  </div>
                  <!-- PASO 2 -->
                  <div class="col-xs-12 col-sm-6 sec-col-01 paso2">
                     <div class="col-xs-2">
                        <div class="paso1 text-right">Paso 2</div>

                     </div>
                     <div class="col-xs-10 col-sm-10 ingresa_institucion">
                        <div class="row">
                           <h3>¿Ya tenés tu cuenta?<br>&nbsp;</h3>
                        </div>
                     </div>
                     <div class="col-xs-12 botonesdescarga container">
                        <a href="https://portal.office.com/OLS/MySoftware.aspx" target="blank" title="CTA Descargá Office Ahora">
                           <div class="descarga_office  text-left">DESCARGÁ OFFICE AHORA</div>
                        </a>
                     </div>

                     <div class="signo text-right paso3" ><img src="images/arrow_red.png" width="100%" style=" " alt="Flecha paso 3" ></div>

                  </div>

                  <div class="col-xs-12 col-sm-12 sec-col-03 collapse" id="collapseExample">
                           <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 col-sm-offset-1 img-loca">
                              <div class="row">
                                 <img src="images/ilus_rotator.png" alt="Ilustración 365" class="img-responsive imagen-ilustracion2 desktop">
                                 <img src="images/ilus_rotator_mobile.png" alt="Ilustración mobile" class="img-responsive imagen-ilustracion2 mobile" style="display: none;"></div>
                           </div>
                           <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10 desc">
                                 <a href="" class="x_sgnopas">X</a><p>Recorda poner correctamente tu cuenta institucional. <br><span>(Por ej:juan@miuniversidad.edu.ar)</span></p>
                                 <ul>
                                    <li>
                                       Si tu institucion ya cuenta con el beneficio de Office 365, pedi tu correo academico al area de Informatica para poder descargarlo gratis.
                                    </li>
                                    <li>
                                       Si tu institucion todavia no cuenta con el beneficio de Office 365, solicita que lo pidan para obtener tu Office gratis.
                                    </li>
                                 </ul>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 sec-col-03 collapse" id="collapseExample2">
                          <div class="row">
                              <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 col-sm-offset-1 img-loca">

                              <div class="row">
                                 <a href="http://aka.ms/quieromioffice-ppt"><img src="images/pc.jpg" alt="Ilustración PC" class="img-responsive imagen-ilustracion2 desktop" style="margin-top:25px; "></a>
                                 <a href="http://aka.ms/quieromioffice-ppt"><img src="images/pc.jpg" alt="Ilustración PC " class="img-responsive imagen-ilustracion2 mobile" style="display: none; margin-top:100% ;"></a></div>
                           </div>
                           <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10  desc1">
                              <a href="" class="x_paso3">X</a>
                                 <h2>¿Ya tenés tu nuevo Office?<br>
                                    ¡Descargá esta presentación que preparamos para vos!</h2>
                           </div>
                          </div>
                        </div>
               </div>
            </div>

         </div>
         
      </div>
      
      <!-- PASOS MOBILE -->
      <section class="pasos_mobile">
         <!-- PASO 1 -->
         <div class="col-xs-12 col-sm-6 sec-col-01">
            <div class="col-xs-2">
               <div class="paso1 text-right">Paso 1</div>
               <div class="signo text-right sgno_pas" id="paso_mobile1" >?</div>
            </div>
            <div class="col-xs-10 col-sm-10 ingresa_institucion">
               <div class="row">
                  <h3>Ingresá tu correo institucional <br>para obtener tu Office</h3>
               </div>
            </div>
            <div class="col-xs-12 botonesdescarga container">
                <a href="https://portal.office.com/start?sku=e82ae690-a2d5-4d76-8d30-7c6e01e6022e" target="_blank" title="haciendo click acá"><div class="descarga_office otro text-left"> HACIENDO CLIC AQUÍ</div></a>

            </div>
         </div>
         <div class="clearfix visible-xs"></div>
         <div class="col-xs-12 col-sm-12 sec-col-03 collapse" id="collapseExample3">
                  <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 col-sm-offset-1 img-loca">
                     <div class="row">
                        <img src="images/ilus_rotator.png" alt="Ilustración Rotator" class="img-responsive imagen-ilustracion2 desktop">
                        <img src="images/ilus_rotator_mobile.png" alt="Ilustración Rotator versión mobile" class="img-responsive imagen-ilustracion2 mobile" style="display: none;"></div>
                  </div>
                  <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10 desc">
                        <p>Recorda poner correctamente tu cuenta institucional. <br><span>(Por ej:juan@miuniversidad.edu.ar)</span></p>
                        <ul>
                           <li>
                              Si tu institucion ya cuenta con el beneficio de Office 365, pedi tu correo academico al area de Informatica para poder descargarlo gratis.
                           </li>
                           <li>
                              Si tu institucion todavia no cuenta con el beneficio de Office 365, solicita que lo pidan para obtener tu Office gratis.
                           </li>
                        </ul>
                  </div>
               </div>
         <!-- PASO 2 -->
         <div class="col-xs-12 col-sm-6 sec-col-01">
            <div class="col-xs-2">
               <div class="paso1 text-right">Paso 2</div>

            </div>
            <div class="col-xs-10 col-sm-10 md-offset-1 ingresa_institucion">
               <div class="row">
                  <h3>¿Ya tenés tu cuenta?</h3>
               </div>
            </div>
            <div class="col-xs-12 botonesdescarga ">
               <a href="https://portal.office.com/OLS/MySoftware.aspx" target="blank">
                  <div class="descarga_office text-left">DESCARGÁ OFFICE AHORA</div>
               </a>
            </div>

            <div class="signo text-right paso3_2"  id="paso_mobile2" ><img src="images/arrow_red.png" width="100%" style="margin-left: 10px; margin-top: -3px; " alt="Flecha Roja"></div>

         </div>
            <div class="col-xs-12 col-sm-12 sec-col-03 collapse" id="collapseExample4">
               <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1 img-loca">
                  <div class="row">
                     <a href="http://aka.ms/quieromioffice-ppt"><img src="images/pc.jpg" alt="Ilustración PC versión mobile" class="img-responsive imagen-ilustracion2 desktop" style="margin-top:25px; "></a>
                     <a href="http://aka.ms/quieromioffice-ppt"><img src="images/pc.jpg" alt="Ilustración PC versión mobile" class="img-responsive imagen-ilustracion2 mobile" style="display: none; margin-top:100% ;"></a></div>
               </div>
               <div class="col-xs-9 col-sm-9 col-md-9 col-lg-10  desc1">
                     <h2>¿Ya tenés tu nuevo Office?<br>
                        ¡Descargá esta presentación que preparamos para vos!</h2>
               </div>
            </div>
      </section>
      <!-- FIN PASOS MOBILE -->
         <!-- rotator -->
             <div class="container-fluid rotator-facultades">
               <div class="container">
                  <ul>
                        <div class="logos"><img src="images/uni/01.png" class="img-reponsive"></div><!--
                        <div class="logos"><img src="images/uni/02.png" class="img-reponsive"></div> -->
                        <div class="logos"><img src="images/uni/32.png" class="img-responsive"></div>
                        <!-- <div class="logos"><img src="images/uni/03.png" class="img-reponsive"></div> -->
                        <div class="logos"><img src="images/uni/31.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/04.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/05.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/06.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/07.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/08.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/09.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/10.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/11.png" class="img-reponsive"></div>
                        <div class="logos"><img src="images/uni/12.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/13.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/14.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/15.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/16.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/17.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/18.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/19.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/20.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/21.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/22.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/23.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/24.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/25.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/26.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/27.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/28.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/29.png" class="img-responsive"></div>
                        <div class="logos"><img src="images/uni/30.png" class="img-responsive"></div>
                  </ul>
               </div>
            </div>
      <!-- Fin Rotator -->
      <!-- beneficios -->
      <!-- ONEDRIVE DESKTOP -->
      <div class="container-fluid onedrivec onedrive-desktop">
             <div class="col-xs-12  beneficios clearfix ">
               <div class="col-xs-12 col-sm-9 disfruta-todoOffice text-center">
                  <h3>OneDrive. <div class="lala3"><br></div> Tu espacio para guardar lo que quieras.</h3>
                  <p>¡Tu nuevo Office viene con OneDrive for Business! Vas a tener 1 TeraByte de almacenamiento <span class="lala4"><br></span>en la Nube, lo que significa que podras guardar, sincronizar y compartir tus archivos <span class="lala4"><br></span>y entretenimiento desde cualquier lugar y en todo momento.</p>
                  <img src="images/img_disfrutaOffice.png" alt="Disfruta office" class="benf1 img-responsive mobile" style="display:none;">
               </div>
               <div class="col-xs-4 col-md-3 laimagen">
                  <img src="images/img_disfrutaOffice.png" alt="Disfruta office" class="benf1 img-responsive desktop">
               </div>
            </div>
      </div>
      <!-- FIN ONEDRIVE DESKTOP -->
      <!-- ONEDRIVE MOBILE -->
      <div class="container-fluid onedrivec onedrive-mobile">
             <div class="col-xs-12  beneficios clearfix ">
               <div class="disfruta-todoOffice ">
                  <h3 class="text-center">OneDrive.<br>Tu espacio para guardar lo que quieras.</h3>
                  <img src="images/img_disfrutaOffice.png" alt="Disfruta office" style="float: right;">
                 ¡Tu nuevo Office viene con OneDrive for BUsiness! Vas a tener 1 TeraByte de almacenamiento en la Nube, lo que significa que podras guardar, sincronizar y compartir tus archivos y entretenimiento desde cualquier lugar y en todo momento.

               </div>
            </div>
      </div>
      <!-- FIN ONEDRIVE MOBILE -->
   <!-- Fin beneficios -->
   <!-- botonera -->
      <div class="container-fluid">
         <!-- BOTONERA DESKTOP -->
         <div class="descarga_y_gana active botonera_version_desktop">
            <div class="col-md-12 col-xs-12 ">
                  <div class="row">
               <a href="#" data-toggle="#div1" class="col-md-2 col-xs-4  office " title="Botón Office"></a>
               <a href="#" data-toggle="#div2" class="col-md-2 col-xs-4 outlook" title="Botón Outlook"></a>
               <a href="#" data-toggle="#div3" class="col-md-2 col-xs-4 sharepoint" title="Botón Sharepoint"></a>
               <a href="#" data-toggle="#div4" class="col-md-2 col-xs-4 skype" title="Botón Skype"></a>
               <a href="#" data-toggle="#div5" class="col-md-2 col-xs-4 onedrive onedrive_1" title="Botón OneDrive"></a>
               <a href="#" data-toggle="#div6" class="col-md-2 col-xs-4 yammer" title="Botón Yammer"></a>
                  </div>
            </div>
         </div>
         <!-- FIN BOTONERA DESKTOP -->
         <!-- BOTONERA MOBILE -->
         <div class="descarga_y_gana active botonera_version_mobile">
            <div class="col-md-12 col-xs-12 ">
                  <div class="row">
               <a href="#" data-toggle="#div1" class="col-md-2 col-xs-4  office" title="Botón Office"></a>
               <a href="#" data-toggle="#div2" class="col-md-2 col-xs-4 outlook" title="Botón Outlook"></a>
               <a href="#" data-toggle="#div3" class="col-md-2 col-xs-4 sharepoint" title="Botón Sharepoint"></a>
               <a href="#" data-toggle="#div4" class="col-md-2 col-xs-4 skype" title="Botón Skype"></a>
               <a href="#" data-toggle="#div5" class="col-md-2 col-xs-4 onedrive onedrive_1" title="Botón OneDrive"></a>
               <a href="#" data-toggle="#div6" class="col-md-2 col-xs-4 yammer" title="Botón Yammer"></a>
                  </div>
            </div>
         </div>
         <!-- FIN BOTONERA MOBILE -->
         <div class="container-fluid botonera" >
            <div class="col-sm-12 ">
               <div class="no-margin office_mix_texto pestaña  ocultar-pest"  id="div1">
                  <!-- <img src="images/logos_office_desplegable.png" alt="logos_office_desplegable" > -->
               </div>
            </div>
            <div class="col-sm-12 sharepoint_mix_texto pestaña ocultar-pest"  id="div2">
               <div class="no-margin text-center">
                  <h3>Organizá tu correo electrónico, coordiná en el calendario los horarios de tus clases y mantené actualizados todos tus contactos. <br>Tenés hasta 50GB de espacio.</h3>
               </div>
            </div>
            <div class="col-sm-12 sharepoint_mix_texto pestaña ocultar-pest"  id="div3">
               <div class="no-margin text-center">
                  <h3>Creación y diseño de webs internas – Almacenamiento de archivos, contactos, calendarios y tareas grupales. <br>
                     Creación y diseño de sitios internos.
                  </h3>
               </div>
            </div>
            <div class="col-sm-12 skype_mix_texto pestaña ocultar-pest"  id="div4">
               <div class="no-margin text-center">
                  <h3>¡La clave para el éxito de un Trabajo Práctico grupal es la comunicación! Con Skype for Business podés hacer <br>videoconferencias con tus compañeros y amigos, compartir la pantalla y grabar conversaciones.</h3>
               </div>
            </div>
            <div class="col-sm-12 onedrive_mix_texto pestaña  text-center"  id="div5">
               <div class="no-margin text-center">
                  <h3>Este es tu lugar para guardar todo lo que quieras en la Nube! Para que no pierdas ni te olvides ninguna tarea en tu casa. <br>Guardá, sincronizá, usa tus archivos en cualquier dispositivo y compartí. Tenés hasta 1TB de almacenamiento <br>(1TB = 1,7 millones de <img src="images/img-texto.png" alt="Icon Sheet"> ó 419 mil <img src="images/img-foto.png" alt="Icon Pic"> ó 200 mil <img src="images/img-audio.png" alt="Icon Audio"> ó 512 <img src="images/img-video.png" alt="Icon Film">) </h3>
               </div>
            </div>
            <div class="col-sm-12 yammer_mix_texto pestaña ocultar-pest"  id="div6">
               <div class="no-margin text-center">
                  <h3>Red social interna para que puedas conversar, postear trabajos, armar comunidades y compartir contenidos <br>con otros alumnos de tu institución.</h3>
               </div>
            </div>
         </div>
            <!-- office mix -->
            <div class="">
               <div class="col-md-12 office_mix" >
                  <div class="row row-ancho row-ancho1">
                     <div class="col-xs-12  col-sm-4  col-sm-offset-4 col-md-3 col-md-offset-0 logo_conoce">
                        <img src="images/logo-officemix.png" alt="Office Mix" class="img-responsive logo_conoce_img centrado logo_conoce_img1"><p>Office Mix</p>
                     </div>
                     <div class="col-xs-12  col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0 mix-sway-one-texts mix-sway-one-texts1">
                        <p>Transformá tus presentaciones de PowerPoint en lecciones interactivas online.
                           Incluye audio y video, grabaciones de pantalla, páginas web y mucho más.</p>
                     </div>
                     <!-- conoce mas desktop -->
                     <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-0 text-center conoce_mas">CONOCÉ MÁS<br><a href="http://www.mix.office.com" target="_blank" class="posicion-plus" ><img src="images/plus_rojo.png" alt="Conocé más Office Mix" class="posicion-plus1" ></a></div>
                     <!-- fin conoce mas desktop -->
                     <!-- conoce mas mobile -->
                      <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-0 text-center conoce_mas_mobile"><p>CONOCÉ MÁS</p><a href="http://www.mix.office.com" target="_blank" class="posicion-plus" ><img src="images/plus_rojo.png" alt="Conocé más Office Mix" class="posicion-plus1" width="25"></a></div>
                      <!-- fin conoce mas mobile -->
                  </div>
               </div>
               <div class="col-md-12 espacio_logos"></div>
               <!-- sway -->
               <div class="col-md-12 swan">
                  <div class="row row-ancho">
                     <div class="col-xs-12 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-0 logo_conoce"><img src="images/logo-sway.png" alt="Sway" class="img-responsive logo_conoce_img"><p>Sway</p>
                     </div>
                     <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0 mix-sway-one-texts">
                        <p>Presentá tus ideas con todos tus dispositivos. Podés incluir imágenes, video,
                        texto para una presentación increíble.</p>
                     </div>
                      <!-- conoce mas desktop   -->
                     <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2  col-md-offset-0 text-center conoce_mas">CONOCÉ MÁS<br><a href="https://www.youtube.com/watch?v=K1ClskG1Pa8" target="_blank" class="posicion-plus" ><img src="images/plus_verde.png" alt="Conocé más Sway" class="posicion-plus1"></a>
                     </div>
                      <!-- fin conoce mas desktop   -->
                       <!-- conoce mas mobile -->
                      <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-0 text-center conoce_mas_mobile"><p>CONOCÉ MÁS</p><a href="http://www.mix.office.com" target="_blank" class="posicion-plus" ><img src="images/plus_rojo.png" alt="Conocé más Sway" class="posicion-plus1" width="25" ></a></div>
                      <!-- fin conoce mas mobile -->
                  </div>
               </div>
               <div class="col-md-12 espacio_logos"></div>
               <!-- one note -->
               <div class="col-md-12 one_note">
                  <div class="row row-ancho">
                     <div class="col-xs-12 col-sm-4 col-sm-offset-4 col-md-3 col-md-offset-0  logo_conoce"><img src="images/logo-onenote.png" alt="One Note" class="img-responsive logo_conoce_img"><p>OneNote</p>
                     </div>
                     <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0 mix-sway-one-texts">
                        <p>Podés llevar este cuaderno digital a todas partes y acceder desde todos tus dispositivos.
                        Compartí tus notas con tus compañeros y editalas en conjunto.</p>                 </div>
                     <!-- conoce mas desktop -->
                     <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-0 text-center conoce_mas">CONOCÉ MÁS<br><a href="http://www.onenote.com" target="_blank" class="posicion-plus" ><img src="images/plus_violeta.png" alt="Conocé más One Note" class="posicion-plus1" ></a>
                    </div>
                  </div>
                  <!-- conoce mas desktop -->
                   <!-- conoce mas mobile -->
                      <div class="col-xs-6 col-xs-offset-6  col-sm-4 col-sm-offset-4 col-md-2 col-md-offset-0 text-center conoce_mas_mobile"><p>CONOCÉ MÁS</p><a href="http://www.mix.office.com" target="_blank" class="posicion-plus" ><img src="images/plus_rojo.png" alt="Conocé más One Note" class="posicion-plus1" width="25" ></a></div>
                  <!-- fin conoce mas mobile -->
               </div>
            </div>
         </div>
         <div class="container-fluid">
            <div class="col-xs-12 col-sm-6 windows10">
               <h3><img src="images/windows10.png" alt="Logo Windows 10"></h3>
               <p><b>Windows 10 está de festejo: cumplimos un año juntos y lo celebramos con la Actualización Aniversario.</b></p>
               <p>La <b>Actualización Aniversario</b> es el update más significativo hasta la fecha desde que se lanzó <b>Windows 10</b>. Está repleto de nuevas funcionalidades que desarrollamos escuchando a nuestros clientes – todas incluidas de manera gratuita. <b>Windows 10</b> continua ayudando a las personas a ser más productivas, mantenerse seguras en línea y por supuesto, divertirse.</p>
               <a href="https://www.microsoft.com/es-es/software-download/windows10" target="_blank" title="Actualizá a Windows 10 gratis">Actualizá aquí</a>
            </div>
            <div class="col-xs-12 col-sm-6 microsoft_azure">
               <div class="nube-br">
                  <div class="nube-bl">
                     <div  class="nube-tl">
                        <h3><img src="images/azure.png" alt="Logo Azure" ></h3>
                        <p>Obtené acceso a la Nube para subir tus proyectos, sitios web, desarrollar aplicaciones
                           y más. Entrá a <strong><a href="http://www.azureboxes.com" class="sin-estilo" target="_blank" title="AzureBoxes">Azureboxes.com</a></strong> y conocé todo lo que podés hacer con Azure.
                        </p>
                        <img src="images/img-azure.png" alt="Imágen Azure" class="img-responsive microsoft_azure_img">
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <!-- Conoce más beneficios DESKTOP -->
      <div class="version_desktop">
         <div class="col-md-12 conoce_beneficios text-right">
            <h3>Conocé más beneficios para estudiantes:  </h3>
         </div>
         <div class=" cuadro_beneficios">
            <div class="col-xs-4 col-sm-4 col-md-4 cuadro_beneficios1">
               <img src="images/bg_beneficios.png" alt="Ilustración Beneficios" class="imagen-beneficios">
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
               <div class="row margin-top-cuadro linkConoceMas">

                  <a href="http://www.azureboxes.com" target="_blank"  title="AzureBoxes">AzureBoxes</a>
                  <a href="http://www.microsoft.com/bizspark" target="_blank" title="Bizspark">Bizspark</a>
                  <a href="http://kodulab.azurewebsites.net/" target="_blank"  title="Kodu">Kodu </a>
              </div>
              <div class="row padding-top linkConoceMas2" >
                  <a href="http://www.dreamspark.com" target="_blank" title="Dreamspark">Dreamspark</a>
                  <a href="http://officeboxes.azurewebsites.net/" target="_blank" title="OfficeBoxes">OfficeBoxes</a>
              </div>
            </div>
         </div>
      </div>
      <!-- FIN Conoce más beneficios DESKTOP -->
      <!-- BENEFICIOS MOBILE-->
      <div class="version_mobile">
         <div class="col-md-12 conoce_beneficios text-right">
            <h3>Conocé más beneficios:  </h3>
         </div>
         <div class=" cuadro_beneficios">
            <div class="col-xs-3 col-sm-4 col-md-4 cuadro_beneficios1">
               <img src="images/bg_beneficios.png" alt="Imágen Beneficios" class="imagen-beneficios">
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8">
               <div class="row margin-top-cuadro linkConoceMas">
                  <a href="http://www.azureboxes.com" target="_blank"  title="AzureBoxes">AzureBoxes</a>
                  <a href="http://www.microsoft.com/bizspark" target="_blank" title="Bizspark">Bizspark</a>
                  <a href="http://kodulab.azurewebsites.net/" target="_blank"  title="Kodu">Kodu </a>
              </div>
              <div class="row padding-top linkConoceMas2" >
                  <a href="http://www.dreamspark.com" target="_blank" title="Dreamspark">Dreamspark</a>
                  <a href="http://officeboxes.azurewebsites.net/" target="_blank" title="OfficeBoxes">OfficeBoxes</a>
              </div>
            </div>
         </div>
      </div>
      <!-- FIN BENEFICIOS MOBILE-->
      <!-- MODAL -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-body">
                  <div class="row">
                     <div class="col-sm-5 cuadro_contactanos">
                        <p>¿Tenés preguntas? <span id="contactanos">Contactanos</span></p>
                     </div>
                     <div class="col-sm-7 cuadro_contactanos2">
                        <p id="mas_info">Para más información,<br><span>escribinos a <b>mioffice@microsoft.com</b> </span></p>
                     </div>
                  </div>
               </div>
               <div class="modal-footer">
                  <div class="text-center">
                     <a href="mailto:mioffice@microsoft.com">
                        <button type="button" class="btn btn-default">Envia Tu Consulta</button>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--FIN MODAL -->
      <!-- LEGALES: oferta válida -->
     <!--   <div class="col-xs-12 legales">
         <p><b>*</b>OFERTA VÁLIDA PARA DISPOSITIVOS CON WINDOWS 7 SP1 Y WINDOWS 8.1 POR UN AÑO DESDE EL 29 DE JULIO DE 2015. LOS TÉRMINOS Y CONDICIONES APLICABLES A LA REFERIDA ACTUALIZACIÓN SE ENCUENTRAN DISPONIBLES EN: <a style="color: #000;     font-family: wf_segoe-ui_semibold;" href="https://www.microsoft.com/es-es/software-download/windows10" target="_blank">AKA.MS/WIN10ACTUALIZACION</a>. CONSULTAR REQUERIMIENTOS TÉCNICOS PARA ACTUALIZAR A WINDOWS 10 <a href="http://www.microsoft.com/es-ar/windows/windows-10-upgrade" target="_blank"><u>AQUÍ</u></a>.</p>
       </div> -->
      <!-- FIN LEGALES: oferta válida -->

      <!-- FOOTER -->
   <footer>
      <div class="container foo-mobile">
         <div class="pull-right-1">
            <img src="images/logo_microsoft.png" alt="Powered by Microsoft Azure" width="104">
               <p> &copy; 2016 Microsoft</p>
         </div>
      <div class="links-margin">
         <a href="http://go.microsoft.com/fwlink/p/?LinkID=511196">Contáctenos</a>
         <a href="http://www.microsoft.com/privacystatement/es-xl/core/default.aspx">Privacidad</a>
         <a href="http://products.office.com/es-ar/products">Todos los productos</a>
         <a href="http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx">Marcas Registradas</a>
         <a href="http://go.microsoft.com/fwlink/p/?LinkID=511200">Sobre nuestra publicidad</a>
      </div>
      </div>
  </footer>
   <!-- FIN FOOTER -->

      <!--JAVASCRIPTS-->
      <script src="assets/jquery.2.1.3.min.js"></script><!--  JQUERY 2.1.3-->
      <script src="assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="assets/js/app.js"></script>
       <script src="funciones_validarform.js"></script>
      <script src="assets/ajax.js"></script>
      <script src="assets/js/owl.carousel.min.js"></script>
      <script>
      $(document).ready(function() {
           $(".rotator-facultades ul").owlCarousel({
               autoPlay: 3000, //Set AutoPlay to 3 seconds
               items: 4,
               itemsDesktop: [1199,3],
               itemsDesktopSmall: [979,3],
               itemsTablet:  [768,2],
               itemsMobile:  [479,2],
               navigation: true,
               pagination: false

           });
      });
      </script>



      <!-- Google Analytics -->
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-74673865-1', 'auto');
        ga('send', 'pageview');

      </script>
      <!-- Fin Google Analytics -->
       <!-- Facebook Pixel Code -->
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','//connect.facebook.net/en_US/fbevents.js');

      fbq('init', '485341735003297');
      fbq('track', "PageView");</script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=485341735003297&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
      <!-- INICIO WEDECX & GOOGLE CODE  -->
      <script type="text/javascript">
         var varSegmentation = 0;var varClickTracking = 1;var varCustomerTracking = 1;var varAutoFirePV =1;var Route="";var Ctrl=""
         document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
      </script>
      <noscript><img src="http://c.microsoft.com/trans_pixel.aspx" id="ctl00_MscomBI_ctl00_ImgWEDCS" width="1" height="1" /></noscript>
      <!--  FIN WEDECX & GOOGLE CODE -->
   </body>
</html>
