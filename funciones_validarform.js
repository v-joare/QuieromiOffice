$(document).ready(function () {
    $(".cta-enviar").click(function (){
		function validateEmail(campoEmail) {
			var filtro = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
			if (filtro.test(campoEmail)) {
				return true;
			}
			else {
				return false;
				}
		}
        $(".error").remove();
        if( $("#nombre").val() == "" ){
            $("#nombre").focus().after("<span class='error'>Ingrese su nombre</span>");
            return false;
        }else if(validateEmail($("#email").val()) == false){
            $("#email").focus().after("<span class='error'>Ingrese un mail de contacto válido</span>");
            return false;
        }else if( $("#institucion").val() == ""){
            $("#institucion").focus().after("<span class='error'>Ingrese su institución</span>");
            return false;
        }else if( $("#apellido").val() == "" ){
            $("#apellido").focus().after("<span class='error'>Ingrese su apellido</span>");
            return false;
        }else if($("#telefono").val() == ""){
            $("#telefono").focus().after("<span class='error'>Ingrese un numero de teléfono valido</span>");
            return false;
        }else if($("#cargo").val() == ""){
            $("#cargo").focus().after("<span class='error'>Ingrese su cargo</span>");
            return false;
        }
    });
});