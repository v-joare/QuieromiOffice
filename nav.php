<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#desplegable" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand menuotro">
         <img src="images/logo_office.png" id="margin-top" alt="Logo Office 365" class="margin-logo-navb">
      </a>
    </div>

    <div class="collapse navbar-collapse" id="desplegable">
      <ul class="nav navbar-nav">
        <li class="activa">
            <a href="index.php" title="Home">Home</a>
        </li>
        <li class="activa2">
            <a href="preguntasFrecuentes.php" title="">Preguntas frecuentes</a>
        </li>
        <li class="activa3">
            <a href="" id="idContacto" data-toggle="modal" data-target="#myModal" title="Contacto">Contacto</a>
        </li>
        <li class="activa4">
            <a href="formulario.php">Office gratis para instituciones</a>
            <span class="imagen_cta">
              <img src="images/arrow.png" alt="flechita" class="img-responsive">
            </span>
        </li>
      </ul>

    </div>
  </div>
</nav>